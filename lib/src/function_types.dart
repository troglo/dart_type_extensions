/// A function type which accepts no parameters and returns nothing. This is
/// synonymous with the builtin [VoidCallback]
typedef NullaryEffect = void Function();

/// A function type which accepts no parameters and returns a value [T]
typedef NullaryFn<T> = T Function();

/// A function type which accepts one parameter [A] and return nothing
typedef UnaryEffect<A> = void Function(A);

/// A function type which accepts on parameter [A] and returns a value [T]
typedef UnaryFn<A, T> = T Function(A);

/// A function type which accepts two parameters [A] and [B] and returns
/// nothing
typedef BinaryEffect<A, B> = void Function(A, B);

/// A function type which accepts two parameters [A] and [B] and returns a
/// value [T]
typedef BinaryFn<A, B, T> = T Function(A, B);

/// A function type which accepts three parameters [A], [B], and [C] and
/// returns nothing
typedef TernaryEffect<A, B, C> = void Function(A, B, C);

/// A function type which accepts three parameters [A], [B], and [C] and
/// returns a value [T]
typedef TernaryFn<A, B, C, T> = T Function(A, B, C);
