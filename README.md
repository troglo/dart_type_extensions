A small dart package which provides a cleaner interface for dealing with
function types.

The purpose of this package is to provide an nicer when developing and using
(through type hints) for interfaces which require higher order functions

The convention followed throughout is that the parameters are given as type
parameters in order with the final parameter being the return type (if not an
effect).

## Usage

These two type annotations are synonymous, however the latter provides a
clearer representation of the intention and easier to read code hint as the
annotation follows the logical flow of the function

```dart
  // without type extensions
  functionWithCallback(double Function(int, int) fn);

  // with type extensions
  functionWithCallback(TernaryFn<int, int, double> fn);
```

## API

### NullaryEffect

A function type which accepts no parameters and returns nothing. This is
synonymous with the builtin [VoidCallback]

### NullaryFn

A function type which accepts no parameters and returns a value [T]

### UnaryEffect

A function type which accepts one parameter [A] and return nothing

### UnaryFn

A function type which accepts on parameter [A] and returns a value [T]

### BinaryEffect

A function type which accepts two parameters [A] and [B] and returns nothing

### BinaryFn

A function type which accepts two parameters [A] and [B] and returns a value
[T]

### TernaryEffect

A function type which accepts three parameters [A], [B], and [C] and returns
nothing

### TernaryFn

A function type which accepts three parameters [A], [B], and [C] and returns a
value [T]

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/troglo/dart_type_extensions/issues
