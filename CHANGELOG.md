## 0.0.2

- Add example
- Improve readme
- Fix links in pubspec and readme

## 0.0.1

Adds the function type aliases:

- NullaryFn
- NullaryEffect
- UnaryFn
- UnaryEffect
- BinaryFn
- BinaryEffect
- TernaryFn
- TernaryEffect
