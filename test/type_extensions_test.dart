import 'package:type_extensions/type_extensions.dart';
import 'package:test/test.dart';

void main() {
  // there are no assertions in this file because we're just
  // checking that everything compiles
  test('NullaryEffect', () {
    void acceptNullaryEffect(NullaryEffect fn) {}
    acceptNullaryEffect(() {});
  });

  test('NullaryFn', () {
    void acceptNullaryFn(NullaryFn<int> fn) {}
    acceptNullaryFn(() => 1);
  });

  test('UnaryEffect', () {
    void acceptUnaryEffect(UnaryEffect<int> fn) {}
    acceptUnaryEffect((int i) {});
  });

  test('UnaryFn', () {
    void acceptUnaryFn(UnaryFn<int, double> fn) {}
    acceptUnaryFn((int a) => a.toDouble());
  });

  test('BinaryEffect', () {
    void acceptBinaryEffect(BinaryEffect<int, int> fn) {}
    acceptBinaryEffect((int a, int b) {});
  });

  test('BinaryFn', () {
    void acceptBinaryFn(BinaryFn<int, double, String> fn) {}
    acceptBinaryFn((int a, double b) => (a / b).toString());
  });

  test('TernaryEffect', () {
    void acceptTernaryEffect(TernaryEffect<int, double, String> fn) {}
    acceptTernaryEffect((int a, double b, String c) {});
  });

  test('TernaryFn', () {
    void acceptTernaryFn(TernaryFn<int, double, String, String> fn) {}
    ;
    acceptTernaryFn((int a, double b, String c) => '$a $b $c');
  });
}
