import 'dart:io';
import 'package:type_extensions/type_extensions.dart';

// this is much easier to read than
// void run(bool Function(DateTime) fn)
void runApp(UnaryFn<DateTime, bool> app) {
  final isSuccess = app(DateTime.now());
  if (isSuccess) {
    exit(0);
  } else {
    exit(1);
  }
}

void main() {
  runApp((startTime) {
    print('Programme started at $startTime');
    return true;
  });
}
